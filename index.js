let display = document.getElementById("display");

let buttons = Array.from(document.getElementsByClassName("button"));


buttons.map((button) => {
  button.addEventListener("click", (e) => {

    switch (e.target.innerText) {
      case "C":
        display.innerText = " ";
        break;
      case "←":
        if (display.innerText) {
          display.innerText = display.innerText.slice(0, -1);
        }
        break;
      case "π":
        display.innerText += "3.14";
        break;

      case "2nd":
        break;
      case "nd":
        break;
      case "exp":
        display.innerText = Math.exp(parseInt(display.innerText));
        break;
      case "1/x":
        display.innerText = 1 / parseInt(display.innerText);
        break;

      case "F-E":
        display.innerText = Number(display.innerText).toExponential();
        break;
      case "x³":
        display.innerText = Math.pow(parseInt(display.innerText), 3);
        break;
      case "x²":
        display.innerText = Math.pow(parseInt(display.innerText), 2);
        break;
      case "3√x":
        display.innerText = Math.cbrt(parseInt(display.innerText));
        break;

      case "2ˣ":
        display.innerText = Math.pow(2, parseInt(display.innerText));
        break;
      case "log₂":
        display.innerText = Math.log2(parseInt(display.innerText));
        break;
      case "eˣ":
        display.innerText = Math.pow(2.7182, parseInt(display.innerText));
        break;
      case "2√x":
        display.innerText = Math.sqrt(parseInt(display.innerText));
        break;
      case "x^y":
        display.innerText += "**";
        break;
      case "10ˣ":
        display.innerText = Math.pow(10, parseInt(display.innerText));
        break;
      case "log":
        display.innerText = Math.log(parseInt(display.innerText));
        break;
      case "ln":
        display.innerText = Math.log(parseInt(display.innerText));
        break;

      case "n!":
        fact();
        // setTimeout(fact, 2000);

        break;
      case "RAD":
        break;
      case "DEG":
        break;
      case "MS":
        break;
      case "M+":
        break;
      case "M-":
        break;
      case "MR":
        break;
      case "MC":
        break;
      case "√":
        display.innerText += Math.sqrt(parseInt(display.innerText));

        break;
      case "+/-":
        display.innerText = Math.sign(parseInt(display.innerText));
        break;
      case "e":
        display.innerText += "2.7182";
        break;
      case "|x|":
        display.innerText = parseInt(0 - display.innerText);
        break;
      case "=":
        try {
          display.innerText = eval(display.innerText);
        } catch {
          display.innerText = "Syntax Error!";
        }

        break;
      default:
        display.innerText += e.target.innerText;
    }
  });
});
var togg = true;
function update() {
  if (togg) {
    document.getElementById("x2").innerText = "x³";
    document.getElementById("sqrt").innerText = "3√x";
    document.getElementById("xraisey").innerText = "x^y";
    document.getElementById("10x").innerText = "2ˣ";
    document.getElementById("log").innerText = "log₂";
    document.getElementById("logn").innerText = "eˣ";
    document.getElementById("2nd").style.color = "black";
    document.getElementById("2nd").style.backgroundColor = "#a0b8ff";
    togg = !togg;
  } else {
    document.getElementById("x2").innerText = "x²";
    document.getElementById("sqrt").innerText = "2√x";
    document.getElementById("xraisey").innerText = "x^y";
    document.getElementById("10x").innerText = "10ˣ";
    document.getElementById("log").innerText = "log";
    document.getElementById("logn").innerText = "ln";
    document.getElementById("2nd").style.color = "black";
    document.getElementById("2nd").style.backgroundColor = "white";
    togg = !togg;
  }
}

var mem = [];
function memory(id) {
  switch (id) {
    case "MS":
      mem.unshift(Number(parseInt(display.innerText)));
      break;
    case "MC":
      mem = [];
      break;
    case "M+":
      if (mem[0]) display.innerText = parseInt(display.innerText) + mem[0];
      break;
    case "M-":
      if (mem[0])
        display.innerText = mem[0] - Number(parseInt(display.innerText));
      break;
    case "MR":
      if (mem[0]) display.innerText += mem[0];
      break;
  }
  document.getElementById("memory").innerHTML =
    mem.length === 0 ? "empty memory" : mem[0];
}

var fact = () => {
  var v,
    res = 1;
  v = parseInt(display.innerText);
  for (let i = 1; i < v + 1; i++) {
    res = res * i;
    //console.log(res);
  }
  display.innerText = res;
};
var radtodeg = true;
function deg() {
  if (radtodeg) {
    document.getElementById("change").innerText = "DEG";
    document.getElementById("change").style.color = "black";
    document.getElementById("change").style.backgroundColor = "#a0b8ff";

    radtodeg = !radtodeg;
  } else {
    document.getElementById("change").innerText = "RAD";
    document.getElementById("change").style.color = "black";
    document.getElementById("change").style.backgroundColor = "white";
    radtodeg = !radtodeg;
  }
}

document.getElementById("trig").onchange = function () {
  if (this.value == "sin") {
    sine();
  }
  if (this.value == "cos") {
    cosec();
  }
  if (this.value == "tan") {
    tane();
  }
};
document.getElementById("func").onchange = function () {
  switch (this.value) {
    case "floor":
      display.innerText = Math.floor(parseInt(display.innerText));
      break;
    case "abs":
      display.innerText = Math.abs(parseInt(display.innerText));
      break;
    case "ceil":
      display.innerText = Math.ceil(parseInt(display.innerText));
      break;
  }
};

function sine() {
  if (radtodeg) {
    console.log(display.innerText);
    display.innerText = Math.sin(parseInt(display.innerText));
  } else {
    display.innerText = parseInt(display.innerText) * (Math.PI / 180);
    display.innerText = Math.sin(parseInt(display.innerText));
  }
}

function cosec() {
  if (radtodeg) {
    display.innerText = Math.cos(parseInt(display.innerText));
  } else {
    display.innerText = parseInt(display.innerText) * (Math.PI / 180);
    display.innerText = Math.cos(parseInt(display.innerText));
  }
}
function tane() {
  if (radtodeg) {
    display.innerText = Math.tan(parseInt(display.innerText));
  } else {
    display.innerText = parseInt(display.innerText) * (Math.PI / 180);
    display.innerText = Math.tan(parseInt(display.innerText));
  }
}
